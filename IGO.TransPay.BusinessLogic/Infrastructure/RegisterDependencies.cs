﻿namespace IGO.TransPay.BusinessLogic.Infrastructure
{
    using IGO.TransPay.BusinessLogic.Services;

    using Microsoft.Extensions.DependencyInjection;

    public static class RegisterDependencies
    {
        public static void RegisterBusinessLogicModule(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUsersService, UsersService>();
        }
    }
}