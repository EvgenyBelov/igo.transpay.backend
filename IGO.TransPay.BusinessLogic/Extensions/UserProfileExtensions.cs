﻿namespace IGO.TransPay.BusinessLogic.Extensions
{
    using IGO.TransPay.BusinessLogic.Models;
    using IGO.TransPay.DataAccess.Models.UserProfile;

    public static class UserProfileExtensions
    {
        public static UserProfileEntity ToUserProfile(this UserProfileUpdateDto updateDto)
        {
            return new UserProfileEntity
            {
                Age = updateDto.Age,
                Name = updateDto.Name,
                LastName = updateDto.LastName,
                EmailAddress = updateDto.EmailAddress,
                PhoneNumber = updateDto.PhoneNumber
            };
        }
    }
}