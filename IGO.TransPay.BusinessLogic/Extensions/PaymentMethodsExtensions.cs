﻿namespace IGO.TransPay.BusinessLogic.Extensions
{
    using IGO.TransPay.BusinessLogic.Models;
    using IGO.TransPay.DataAccess.Models.Payment;

    public static class PaymentMethodsExtensions
    {
        public static PaymentMethodEntity ToEntity(this PaymentMethodCreateDto createModel)
        {
            return new PaymentMethodEntity
            {
                CardholderName = createModel.CardholderName,
                ExpDate = createModel.ExpDate,
                IsDefault = createModel.IsDefault,
                CardNumber = createModel.CardNumber
            };
        }

        public static PaymentMethodViewDto ToViewModel(this PaymentMethodEntity paymentMethod)
        {
            return new PaymentMethodViewDto
            {
                CardholderName = paymentMethod.CardholderName,
                ExpDate = paymentMethod.ExpDate,
                IsDefault = paymentMethod.IsDefault,
                CardNumber = paymentMethod.CardNumber.Masked('*', 2, paymentMethod.CardNumber.Length - 4 - 2),
                CreatedAt = paymentMethod.CreatedAt,
                Id = paymentMethod.Id
            };
        }
    }
}