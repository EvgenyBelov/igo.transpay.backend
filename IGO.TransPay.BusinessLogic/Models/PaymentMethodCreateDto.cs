﻿namespace IGO.TransPay.BusinessLogic.Models
{
    public sealed class PaymentMethodCreateDto
    {
        public string CardholderName { get; set; }

        public string CardNumber { get; set; }

        public string ExpDate { get; set; }

        public bool IsDefault { get; set; }
    }
}