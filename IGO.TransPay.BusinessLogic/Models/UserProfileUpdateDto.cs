﻿namespace IGO.TransPay.BusinessLogic.Models
{
    public sealed class UserProfileUpdateDto
    {
        public int? Age { get; set; }

        public string EmailAddress { get; set; }

        public string LastName { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }
    }
}