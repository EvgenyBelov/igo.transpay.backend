﻿namespace IGO.TransPay.BusinessLogic.Models
{
    using System;

    public sealed class PaymentMethodViewDto
    {
        public Guid Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public string CardholderName { get; set; }

        public string CardNumber { get; set; }

        public string ExpDate { get; set; }

        public bool IsDefault { get; set; }
    }
}