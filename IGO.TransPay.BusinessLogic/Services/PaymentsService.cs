﻿namespace IGO.TransPay.BusinessLogic.Services
{
    using System;
    using System.Threading.Tasks;

    using IGO.TransPay.BusinessLogic.Extensions;
    using IGO.TransPay.BusinessLogic.Models;
    using IGO.TransPay.DataAccess.Models.Payment;
    using IGO.TransPay.DataAccess.Models.Results;
    using IGO.TransPay.DataAccess.Repositories;

    public sealed class PaymentsService
    {
        private readonly IPaymentMethodsRepository paymentMethodsRepository;

        public PaymentsService(IPaymentMethodsRepository paymentMethodsRepository)
        {
            this.paymentMethodsRepository = paymentMethodsRepository;
        }

        public async Task<PaymentMethodViewDto> Add(PaymentMethodCreateDto paymentMethod, Guid userId)
        {
            var count = await this.paymentMethodsRepository.GetCount(userId);
            var entity = paymentMethod.ToEntity();
            PaymentMethodEntity createdMethod;
            if (count == 0)
            {
                entity.IsDefault = true;
                createdMethod = await this.paymentMethodsRepository.Add(entity, userId);
            }
            else
            {
                createdMethod = await this.paymentMethodsRepository.Add(entity, userId);
                await this.paymentMethodsRepository.SetDefaultMethod(createdMethod.Id, userId);
            }

            return createdMethod.ToViewModel();
        }

        public Task Delete(Guid paymentMethodId, Guid userId)
        {
            return null;
        }

        public Task<PagedResult<PaymentMethodEntity>> GetByUser(PagedRequest request, Guid userId)
        {
            return this.paymentMethodsRepository.GetByUser(request, userId);
        }
    }
}