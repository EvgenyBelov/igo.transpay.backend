﻿namespace IGO.TransPay.BusinessLogic.Services
{
    using System;
    using System.Threading.Tasks;

    using IGO.TransPay.BusinessLogic.Extensions;
    using IGO.TransPay.BusinessLogic.Models;
    using IGO.TransPay.DataAccess.Models.UserProfile;
    using IGO.TransPay.DataAccess.Repositories;

    internal sealed class UsersService : IUsersService
    {
        private readonly IUsersRepository usersRepository;

        public UsersService(IUsersRepository usersRepository)
        {
            this.usersRepository = usersRepository;
        }

        public async Task<UserProfileEntity> GetOrCreate(string deviceToken)
        {
            using (this.usersRepository)
            {
                var user = await this.usersRepository.Get(deviceToken);
                if (user == null)
                {
                    user = await this.usersRepository.Create(deviceToken, new UserProfileEntity());
                }

                return user;
            }
        }

        public Task<Guid> Update(string deviceToken, UserProfileUpdateDto userProfile)
        {
            return this.usersRepository.Update(deviceToken, userProfile.ToUserProfile());
        }
    }
}