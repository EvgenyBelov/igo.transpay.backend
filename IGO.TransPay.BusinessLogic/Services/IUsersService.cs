﻿namespace IGO.TransPay.BusinessLogic.Services
{
    using System;
    using System.Threading.Tasks;

    using IGO.TransPay.BusinessLogic.Models;
    using IGO.TransPay.DataAccess.Models.UserProfile;

    public interface IUsersService
    {
        Task<UserProfileEntity> GetOrCreate(string deviceToken);

        Task<Guid> Update(string deviceToken, UserProfileUpdateDto userProfile);
    }
}