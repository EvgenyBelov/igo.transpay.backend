﻿namespace IGO.TransPay.Controllers
{
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.Repositories;
    using IGO.TransPay.Models;

    using Microsoft.AspNetCore.Mvc;

    [Route("v1/[controller]")]
    [ApiController]
    public sealed class TransportsController : IdentityController
    {
        private readonly ITransportsRepository transportsRepository;

        public TransportsController(ITransportsRepository transportsRepository)
        {
            this.transportsRepository = transportsRepository;
        }

        /// <summary>
        /// Get filtered transports.
        /// </summary>
        /// <param name="request">Filtering request (Page, Take)</param>
        /// <returns>
        /// List of transports.
        /// </returns>
        [HttpGet]
        public async Task<IActionResult> All([FromQuery]PagedRequest request)
        {
            var transports = await this.transportsRepository.Get(
                                 new DataAccess.Models.Results.PagedRequest(request.Page, request.Take));

            return this.Ok(transports);
        }

        /// <summary>
        /// Finds the transport by route's name.
        /// </summary>
        /// <param name="request">Filtering request (Page, Take)</param>
        /// <param name="routeName">Name of the route.</param>
        /// <returns>
        /// Transport serving the route.
        /// </returns>
        [HttpGet("route")]
        public async Task<IActionResult> FindRoute([FromQuery]PagedRequest request, string routeName)
        {
            if (string.IsNullOrEmpty(routeName))
            {
                return this.BadRequest("Route is not specified.");
            }

            var transports = await this.transportsRepository.FindRouteByName(
                                 new DataAccess.Models.Results.PagedRequest(request.Page, request.Take), routeName);

            return this.Ok(transports);
        }
    }
}