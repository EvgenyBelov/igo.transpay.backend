﻿namespace IGO.TransPay.Controllers
{
    using System;
    using System.Threading.Tasks;

    using IGO.TransPay.BusinessLogic.Models;
    using IGO.TransPay.BusinessLogic.Services;
    using IGO.TransPay.Extensions;
    using IGO.TransPay.Models.User;

    using Microsoft.AspNetCore.Mvc;

    [Route("v1/[controller]")]
    [ApiController]
    public sealed class UserProfileController : IdentityController
    {
        private readonly IUsersService usersService;

        public UserProfileController(IUsersService usersService)
        {
            this.usersService = usersService;
        }

        /// <summary>
        /// Creates or gets a user. Creates a user if the token does not exist in DB, otherwise gets user's from the DB.
        /// </summary>
        /// <returns>
        /// User's profile info.
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> CreateOrGet()
        {
            if (string.IsNullOrEmpty(this.DeviceToken))
            {
                return this.BadRequest("Device was not recognized.");
            }

            var userProfile = await this.usersService.GetOrCreate(this.DeviceToken);
            return new JsonResult(userProfile.ToViewModel());
        }

        /// <summary>
        /// Edit the user's profile info.
        /// </summary>
        /// <param name="editModel">Edited user's profile info.</param>
        [HttpPut]
        public async Task<IActionResult> Update(UserProfileEditModel editModel)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var id = await this.usersService.Update(
                  this.DeviceToken,
                  new UserProfileUpdateDto
                  {
                      Age = editModel.Age,
                      EmailAddress = editModel.EmailAddress,
                      LastName = editModel.LastName,
                      Name = editModel.Name,
                      PhoneNumber = editModel.PhoneNumber
                  });

           if (id != Guid.Empty)
           {
               return this.Ok();
           }

           return this.BadRequest("Could not update user's info.");
        }
    }
}