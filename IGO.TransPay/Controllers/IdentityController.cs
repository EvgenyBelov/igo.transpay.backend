﻿namespace IGO.TransPay.Controllers
{
    using System.Linq;
    using System.Security.Claims;
    using System.Threading;

    using Microsoft.AspNetCore.Mvc;

    public class IdentityController : ControllerBase
    {
        private string deviceToken;

        protected string DeviceToken =>
            LazyInitializer.EnsureInitialized(
                ref this.deviceToken,
                () => this.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid)?.Value);
    }
}