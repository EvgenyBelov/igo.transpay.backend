﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IGO.TransPay.Migrations
{
    public partial class UserProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "trp");

            migrationBuilder.CreateTable(
                name: "Organizations",
                schema: "trp",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Inn = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoutePrices",
                schema: "trp",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoutePrices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserProfiles",
                schema: "trp",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    EmailAddress = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GroundTransportRoutes",
                schema: "trp",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PriceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroundTransportRoutes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroundTransportRoutes_RoutePrices_PriceId",
                        column: x => x.PriceId,
                        principalSchema: "trp",
                        principalTable: "RoutePrices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubwayLines",
                schema: "trp",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PriceId = table.Column<int>(nullable: false),
                    Color = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubwayLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubwayLines_RoutePrices_PriceId",
                        column: x => x.PriceId,
                        principalSchema: "trp",
                        principalTable: "RoutePrices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                schema: "trp",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserProfileId = table.Column<Guid>(nullable: false),
                    DeviceIdentifier = table.Column<string>(nullable: true),
                    ExpirationDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserTokens_UserProfiles_UserProfileId",
                        column: x => x.UserProfileId,
                        principalSchema: "trp",
                        principalTable: "UserProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroundTransports",
                schema: "trp",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    TransportType = table.Column<int>(nullable: false),
                    UniqueIdentifier = table.Column<string>(nullable: true),
                    OrganizationId = table.Column<Guid>(nullable: false),
                    RouteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroundTransports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroundTransports_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "trp",
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroundTransports_GroundTransportRoutes_RouteId",
                        column: x => x.RouteId,
                        principalSchema: "trp",
                        principalTable: "GroundTransportRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UndergroundTransports",
                schema: "trp",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    TransportType = table.Column<int>(nullable: false),
                    UniqueIdentifier = table.Column<string>(nullable: true),
                    OrganizationId = table.Column<Guid>(nullable: false),
                    RouteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UndergroundTransports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UndergroundTransports_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "trp",
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UndergroundTransports_SubwayLines_RouteId",
                        column: x => x.RouteId,
                        principalSchema: "trp",
                        principalTable: "SubwayLines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroundTransportRoutes_PriceId",
                schema: "trp",
                table: "GroundTransportRoutes",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_GroundTransports_OrganizationId",
                schema: "trp",
                table: "GroundTransports",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_GroundTransports_RouteId",
                schema: "trp",
                table: "GroundTransports",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_SubwayLines_PriceId",
                schema: "trp",
                table: "SubwayLines",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_UndergroundTransports_OrganizationId",
                schema: "trp",
                table: "UndergroundTransports",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_UndergroundTransports_RouteId",
                schema: "trp",
                table: "UndergroundTransports",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTokens_UserProfileId",
                schema: "trp",
                table: "UserTokens",
                column: "UserProfileId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroundTransports",
                schema: "trp");

            migrationBuilder.DropTable(
                name: "UndergroundTransports",
                schema: "trp");

            migrationBuilder.DropTable(
                name: "UserTokens",
                schema: "trp");

            migrationBuilder.DropTable(
                name: "GroundTransportRoutes",
                schema: "trp");

            migrationBuilder.DropTable(
                name: "Organizations",
                schema: "trp");

            migrationBuilder.DropTable(
                name: "SubwayLines",
                schema: "trp");

            migrationBuilder.DropTable(
                name: "UserProfiles",
                schema: "trp");

            migrationBuilder.DropTable(
                name: "RoutePrices",
                schema: "trp");
        }
    }
}
