﻿namespace IGO.TransPay.Extensions
{
    using IGO.TransPay.DataAccess.Models.UserProfile;
    using IGO.TransPay.Models.User;

    public static class UserExtensions
    {
        public static UserProfileViewModel ToViewModel(this UserProfileEntity userProfile)
        {
            if (userProfile == null)
            {
                return null;
            }

            return new UserProfileViewModel
            {
                Age = userProfile.Age,
                Name = userProfile.Name,
                EmailAddress = userProfile.EmailAddress,
                LastName = userProfile.LastName,
                PhoneNumber = userProfile.PhoneNumber
            };
        }
    }
}