﻿namespace IGO.TransPay.Auth
{
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Http;

    public sealed class CheckDeviceTokenMiddleware
    {
        private readonly RequestDelegate next;

        public CheckDeviceTokenMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var deviceToken = context.Request.Headers["Device-Token"];

            context.User.AddIdentity(
                new ClaimsIdentity(
                    new[]
                        {
                            new Claim(
                                ClaimTypes.Sid, 
                                deviceToken.ToString() ?? string.Empty)
                        }));

            await this.next(context);
        }
    }
}