﻿namespace IGO.TransPay.Models
{
    public class PagedRequest
    {
        private const int MinItemsPerPage = 5;

        private int page;

        private int take;

        public int Page
        {
            get => this.page;
            set => this.page = value > 0 ? value : 0;
        }

        public int Take
        {
            get => this.take;
            set => this.take = value > MinItemsPerPage ? value : MinItemsPerPage;
        }
    }
}