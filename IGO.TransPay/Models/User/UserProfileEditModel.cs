﻿namespace IGO.TransPay.Models.User
{
    using System.ComponentModel.DataAnnotations;

    public sealed class UserProfileEditModel
    {
        [Range(14, 100)]
        public int? Age { get; set; }

        [EmailAddress]
        public string EmailAddress { get; set; }

        [MaxLength(128)]
        public string LastName { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [Phone]
        public string PhoneNumber { get; set; }
    }
}