﻿namespace IGO.TransPay
{
    using IGO.TransPay.Auth;
    using IGO.TransPay.BusinessLogic.Infrastructure;
    using IGO.TransPay.DataAccess.DataContext;
    using IGO.TransPay.DataAccess.Infrastructure;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    using Swashbuckle.AspNetCore.Swagger;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseMiddleware<CheckDeviceTokenMiddleware>();
            app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "IGO.TransportAPI");
                });

            app.UseMvc();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<DataContext>(
                options => options.UseSqlServer("Server=tcp:dc8w6yxjpm.database.windows.net,1433;Database=yRuler2Dev;User ID=tngdb_widget@dc8w6yxjpm;Password=zeRjV8dXVx;Connection Timeout=120;Max Pool Size=5000;", b => b.MigrationsAssembly("IGO.TransPay")));

            services.Configure<IISOptions>(options =>
                {
                    options.ForwardClientCertificate = false;
                });

            services.AddMvc(options =>
                {
                    options.RespectBrowserAcceptHeader = true;
                });

            services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new Info { Title = "IGO.TransportAPI", Version = "v1" });
                });

            services.RegisterDataAccessModule();
            services.RegisterBusinessLogicModule();
        }
    }
}