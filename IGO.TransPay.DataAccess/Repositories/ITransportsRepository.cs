﻿namespace IGO.TransPay.DataAccess.Repositories
{
    using System;
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.Models.Results;
    using IGO.TransPay.DataAccess.Models.Transport;

    public interface ITransportsRepository
    {
        Task<GroundTransportEntity> GetById(Guid id);

        Task<Guid> Create(GroundTransportEntity transport);

        Task<Guid> Update(Guid id, GroundTransportEntity transport);

        Task<PagedResult<GroundTransportEntity>> Get(PagedRequest request);

        Task<PagedResult<GroundTransportEntity>> FindRouteByName(PagedRequest request, string name);
    }
}