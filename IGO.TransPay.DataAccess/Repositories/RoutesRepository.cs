﻿namespace IGO.TransPay.DataAccess.Repositories
{
    using System.Linq;
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.DataContext;
    using IGO.TransPay.DataAccess.Models.Route;

    using Microsoft.EntityFrameworkCore;

    internal sealed class RoutesRepository : IRoutesRepository
    {
        private readonly TransportContext transportContext;

        public RoutesRepository(TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }
    }
}
