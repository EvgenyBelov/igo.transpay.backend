﻿namespace IGO.TransPay.DataAccess.Repositories
{
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.Models.Route;

    public interface IRoutesRepository
    {
        Task<GroundTransportRoute[]> FindRouteByName(string name);
    }
}