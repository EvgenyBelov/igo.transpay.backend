﻿namespace IGO.TransPay.DataAccess.Repositories
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.DataContext;
    using IGO.TransPay.DataAccess.Models.Results;
    using IGO.TransPay.DataAccess.Models.Transport;

    using Microsoft.EntityFrameworkCore;

    internal sealed class TransportsRepository : ITransportsRepository
    {
        private readonly DataContext dataContext;

        public TransportsRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<GroundTransportEntity> GetById(Guid id)
        {
            using (this.dataContext)
            {
                return await this.GetByIdInternal(id);
            }
        }

        public async Task<Guid> Create(GroundTransportEntity transport)
        {
            using (this.dataContext)
            {
                this.dataContext.GroundTransports.Add(transport);
                await this.dataContext.SaveChangesAsync();
            }

            return transport.Id;
        }

        public async Task<Guid> Update(Guid id, GroundTransportEntity transport)
        {
            using (this.dataContext)
            {
                var entity = await this.GetByIdInternal(id);

                if (entity == null)
                {
                    return Guid.Empty;
                }

                entity.TransportType = transport.TransportType;
                entity.UniqueIdentifier = transport.UniqueIdentifier;
                entity.RouteId = transport.RouteId;
                await this.dataContext.SaveChangesAsync();
            }

            return transport.Id;
        }

        public async Task<PagedResult<GroundTransportEntity>> Get(PagedRequest request)
        {
            using (this.dataContext)
            {
                var query = this.ComposeQueryInternal();

                var total = await query.CountAsync();

                var result = await query
                      .Skip(request.Page * request.Take)
                      .Take(request.Take)
                      .ToArrayAsync();

                return new PagedResult<GroundTransportEntity>(request, result, total);
            }
        }

        public async Task<PagedResult<GroundTransportEntity>> FindRouteByName(PagedRequest request, string name)
        {
            using (this.dataContext)
            {
                var query = this.ComposeQueryInternal().Where(x => x.Route.Name.Contains(name));

                var total = await query.CountAsync();

                var results = await query
                                  .Skip(request.Page * request.Take)
                                  .Take(request.Take)
                                  .ToArrayAsync();

                return new PagedResult<GroundTransportEntity>(request, results, total);
            }
        }

        private async Task<GroundTransportEntity> GetByIdInternal(Guid id)
        {
            return await this.ComposeQueryInternal().FirstOrDefaultAsync(x => x.Id == id);
        }

        private IQueryable<GroundTransportEntity> ComposeQueryInternal()
        {
            return this.dataContext.GroundTransports.Include(x => x.Route);
        }
    }
}
