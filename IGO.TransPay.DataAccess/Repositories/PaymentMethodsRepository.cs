﻿namespace IGO.TransPay.DataAccess.Repositories
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.DataContext;
    using IGO.TransPay.DataAccess.Models.Payment;
    using IGO.TransPay.DataAccess.Models.Results;

    using Microsoft.EntityFrameworkCore;

    internal sealed class PaymentMethodsRepository : IPaymentMethodsRepository
    {
        private readonly DataContext dataContext;

        public PaymentMethodsRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<PaymentMethodEntity> Add(PaymentMethodEntity paymentMethod, Guid userId)
        {
            paymentMethod.UserProfileId = userId;
            this.dataContext.PaymentMethods.Add(paymentMethod);

            await this.dataContext.SaveChangesAsync();

            return paymentMethod;
        }

        public Task<int> GetCount(Guid userId)
        {
            return this.dataContext.PaymentMethods.CountAsync(x => x.UserProfileId == userId);
        }

        public async Task<Guid> SetDefaultMethod(Guid methodId, Guid userId)
        {
            var defaultMethod =
                await this.dataContext.PaymentMethods.FirstAsync(x => x.UserProfileId == userId && x.IsDefault);
            defaultMethod.IsDefault = false;

            var newDefaultMethod =
                await this.dataContext.PaymentMethods.FirstAsync(x => x.UserProfileId == userId && x.Id == methodId);
            newDefaultMethod.IsDefault = true;
            await this.dataContext.SaveChangesAsync();

            return newDefaultMethod.Id;
        }

        public async Task<PagedResult<PaymentMethodEntity>> GetByUser(PagedRequest request, Guid userId)
        {
            var query = this.dataContext.PaymentMethods
                .Where(x => x.UserProfileId == userId)
                .OrderByDescending(x => x.IsDefault)
                .ThenByDescending(x => x.CreatedAt);

            var total = await query.CountAsync();

            var data = await query
                  .Skip(request.Page * request.Take)
                  .Take(request.Take)
                  .ToArrayAsync();

            return new PagedResult<PaymentMethodEntity>(request, data, total);
        }

        public async Task Delete(Guid paymentMethodId, Guid userId)
        {
            var method = await this.dataContext
                .PaymentMethods
                .FirstOrDefaultAsync(x => x.Id == paymentMethodId &&
                                          x.UserProfileId == userId);

            if (method == null)
            {
                return;
            }

            this.dataContext.PaymentMethods.Remove(method);

            await this.dataContext.SaveChangesAsync();
        }
    }
}
