﻿namespace IGO.TransPay.DataAccess.Repositories
{
    using System;
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.Models.Payment;
    using IGO.TransPay.DataAccess.Models.Results;

    public interface IPaymentMethodsRepository
    {
        Task<PaymentMethodEntity> Add(PaymentMethodEntity paymentMethod, Guid userId);

        Task<PagedResult<PaymentMethodEntity>> GetByUser(PagedRequest request, Guid userId);

        Task<int> GetCount(Guid userId);

        Task Delete(Guid paymentMethodId, Guid userId);

        Task<Guid> SetDefaultMethod(Guid methodId, Guid userId);
    }
}