﻿namespace IGO.TransPay.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.DataContext;
    using IGO.TransPay.DataAccess.Models.UserProfile;

    using Microsoft.EntityFrameworkCore;

    internal sealed class UsersRepository : IUsersRepository
    {
        private readonly DataContext dataContext;

        public UsersRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<UserProfileEntity> Create(string deviceToken, UserProfileEntity user)
        {
            user.Tokens = new List<UserTokenEntity>
                                  {
                                      new UserTokenEntity
                                          {
                                              DeviceIdentifier = deviceToken
                                          }
                                  };

            this.dataContext.UserProfiles.Add(user);

            await this.dataContext.SaveChangesAsync();

            return user;
        }

        public async Task<Guid> Update(string deviceToken, UserProfileEntity user)
        {
            var token = await this.GetUserTokenInternal(deviceToken);
            if (token == null)
            {
                return Guid.Empty;
            }

            var profile = await this.GetUserInternal(token.UserProfileId);
            profile.Name = user.Name;
            profile.LastName = user.LastName;
            profile.PhoneNumber = user.PhoneNumber;
            profile.EmailAddress = user.EmailAddress;
            profile.Age = user.Age;
            await this.dataContext.SaveChangesAsync();
            return profile.Id;
        }

        public async Task<UserProfileEntity> Get(string deviceToken)
        {
            var token = await this.GetUserTokenInternal(deviceToken);
            if (token == null)
            {
                return null;
            }

            return await this.dataContext.UserProfiles.FirstOrDefaultAsync(x => x.Id == token.UserProfileId);
        }

        public void Dispose()
        {
            this.dataContext?.Dispose();
        }

        private Task<UserProfileEntity> GetUserInternal(Guid id)
        {
            return this.dataContext.UserProfiles.FirstOrDefaultAsync(x => x.Id == id);
        }

        private Task<UserTokenEntity> GetUserTokenInternal(string deviceToken)
        {
            return this.dataContext.UserTokens
                .OrderByDescending(x => x.ExpirationDate)
                .FirstOrDefaultAsync(x => x.DeviceIdentifier == deviceToken);
        }
    }
}
