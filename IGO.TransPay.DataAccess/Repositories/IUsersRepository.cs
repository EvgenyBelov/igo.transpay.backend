﻿namespace IGO.TransPay.DataAccess.Repositories
{
    using System;
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.Models.UserProfile;

    public interface IUsersRepository : IDisposable
    {
        Task<UserProfileEntity> Create(string deviceToken, UserProfileEntity user);

        Task<Guid> Update(string deviceToken, UserProfileEntity user);

        Task<UserProfileEntity> Get(string deviceToken);
    }
}