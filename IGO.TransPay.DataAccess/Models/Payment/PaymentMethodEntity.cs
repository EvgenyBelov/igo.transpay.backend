﻿namespace IGO.TransPay.DataAccess.Models.Payment
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using IGO.TransPay.DataAccess.Models.Common;
    using IGO.TransPay.DataAccess.Models.UserProfile;

    public sealed class PaymentMethodEntity : DatedEntity
    {
        public string CardholderName { get; set; }

        public string CardNumber { get; set; }

        public string ExpDate { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public bool IsDefault { get; set; }

        public UserProfileEntity UserProfile { get; set; }

        public Guid UserProfileId { get; set; }
    }
}