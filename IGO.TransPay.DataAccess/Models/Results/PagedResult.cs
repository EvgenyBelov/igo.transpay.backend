﻿namespace IGO.TransPay.DataAccess.Models.Results
{
    public sealed class PagedResult<TEntity>
    {
        public PagedResult(PagedRequest request, TEntity[] result, int total)
        {
            this.Entities = result;
            this.Page = request.Page;
            this.Take = request.Take;
            this.Total = total;
        }

        public TEntity[] Entities { get; }

        public int Page { get; }

        public int ResultsCount => this.Entities.Length;

        public int Take { get; }

        public int Total { get; }
    }
}