﻿namespace IGO.TransPay.DataAccess.Models.Results
{
    public sealed class PagedRequest
    {
        public PagedRequest(int page, int take)
        {
            this.Page = page;
            this.Take = take;
        }

        public int Page { get; }

        public int Take { get; }
    }
}