﻿namespace IGO.TransPay.DataAccess.Models.Route
{
    public sealed class SubwayLineEntity : RouteEntity
    {
        public string Color { get; set; }
    }
}