﻿namespace IGO.TransPay.DataAccess.Models.Route
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using IGO.TransPay.DataAccess.Models.Common;

    public abstract class RouteEntity : DatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public int PriceId { get; set; }

        public RoutePriceEntity Price { get; set; }
    }
}