﻿namespace IGO.TransPay.DataAccess.Models.Route
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using IGO.TransPay.DataAccess.Models.Common;

    public sealed class RoutePriceEntity : DatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public decimal Price { get; set; }
    }
}