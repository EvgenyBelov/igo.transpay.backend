﻿namespace IGO.TransPay.DataAccess.Models.Organization
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using IGO.TransPay.DataAccess.Models.Common;

    public sealed class OrganizationEntity : DatedEntity
    {
        public string Address { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Inn { get; set; }

        public string Name { get; set; }
    }
}