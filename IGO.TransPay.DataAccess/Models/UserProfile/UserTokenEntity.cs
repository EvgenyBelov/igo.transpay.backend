﻿namespace IGO.TransPay.DataAccess.Models.UserProfile
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using IGO.TransPay.DataAccess.Models.Common;

    public sealed class UserTokenEntity : DatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Guid UserProfileId { get; set; }

        public UserProfileEntity UserProfile { get; set; }

        public string DeviceIdentifier { get; set; }

        public DateTime? ExpirationDate { get; set; }
    }
}
