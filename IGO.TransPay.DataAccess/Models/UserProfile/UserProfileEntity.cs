﻿namespace IGO.TransPay.DataAccess.Models.UserProfile
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using IGO.TransPay.DataAccess.Models.Common;
    using IGO.TransPay.DataAccess.Models.Payment;

    public sealed class UserProfileEntity : DatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string EmailAddress { get; set; }

        public string LastName { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public int? Age { get; set; }

        public List<UserTokenEntity> Tokens { get; set; }

        public List<PaymentMethodEntity> PaymentMethods { get; set; }
    }
}