﻿namespace IGO.TransPay.DataAccess.Models.Transport
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using IGO.TransPay.DataAccess.Models.Common;
    using IGO.TransPay.DataAccess.Models.Organization;

    public abstract class VehicleEntity : DatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public TransportType TransportType { get; set; }

        public string UniqueIdentifier { get; set; }

        public OrganizationEntity Organization { get; set; }

        public Guid OrganizationId { get; set; }
    }
}