﻿namespace IGO.TransPay.DataAccess.Models.Transport
{
    using IGO.TransPay.DataAccess.Models.Route;

    public sealed class GroundTransportEntity : VehicleEntity
    {
        public GroundTransportRoute Route { get; set; }

        public int RouteId { get; set; }
    }
}