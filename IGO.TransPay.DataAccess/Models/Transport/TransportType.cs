﻿namespace IGO.TransPay.DataAccess.Models.Transport
{
    public enum TransportType
    {
        Bus,

        LongDistanceBus,

        Tram,

        Trolley,

        Subway,

        Taxi
    }
}