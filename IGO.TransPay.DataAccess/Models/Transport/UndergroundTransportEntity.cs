﻿namespace IGO.TransPay.DataAccess.Models.Transport
{
    using IGO.TransPay.DataAccess.Models.Route;

    public sealed class UndergroundTransportEntity : VehicleEntity
    {
        public SubwayLineEntity Route { get; set; }

        public int RouteId { get; set; }
    }
}