﻿namespace IGO.TransPay.DataAccess.Models.Common
{
    using System;

    public interface IDatedEntity
    {
        DateTime CreatedAt { get; set; }

        DateTime UpdatedAt { get; set; }
    }
}