﻿namespace IGO.TransPay.DataAccess.Models.Common
{
    using System;

    public class DatedEntity : IDatedEntity
    {
        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}