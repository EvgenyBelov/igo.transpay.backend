﻿namespace IGO.TransPay.DataAccess.DataContext
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using IGO.TransPay.DataAccess.Models.Common;
    using IGO.TransPay.DataAccess.Models.Organization;
    using IGO.TransPay.DataAccess.Models.Payment;
    using IGO.TransPay.DataAccess.Models.Route;
    using IGO.TransPay.DataAccess.Models.Transport;
    using IGO.TransPay.DataAccess.Models.UserProfile;

    using Microsoft.EntityFrameworkCore;

    public sealed class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<GroundTransportRoute> GroundTransportRoutes { get; set; }

        public DbSet<GroundTransportEntity> GroundTransports { get; set; }

        public DbSet<SubwayLineEntity> SubwayLines { get; set; }

        public DbSet<UndergroundTransportEntity> UndergroundTransports { get; set; }

        public DbSet<UserProfileEntity> UserProfiles { get; set; }

        public DbSet<UserTokenEntity> UserTokens { get; set; }

        public DbSet<PaymentMethodEntity> PaymentMethods { get; set; }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            var addedEntities = ChangeTracker
                .Entries()
                .Where(x => x.State == EntityState.Added).ToList();

            foreach (var addedEntity in addedEntities)
            {
                if (addedEntity.Entity is DatedEntity datedEntity)
                {
                    datedEntity.UpdatedAt = datedEntity.CreatedAt = DateTime.UtcNow;
                }
            }

            var updatedEntities = ChangeTracker
                .Entries()
                .Where(x => x.State == EntityState.Modified).ToList();

            foreach (var updatedEntity in updatedEntities)
            {
                if (updatedEntity.Entity is DatedEntity datedEntity)
                {
                    datedEntity.UpdatedAt = DateTime.UtcNow;
                }
            }

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RoutePriceEntity>().ToTable("RoutePrices", "trp");
            modelBuilder.Entity<GroundTransportEntity>().ToTable("GroundTransports", "trp");
            modelBuilder.Entity<UndergroundTransportEntity>().ToTable("UndergroundTransports", "trp");
            modelBuilder.Entity<GroundTransportRoute>().ToTable("GroundTransportRoutes", "trp");
            modelBuilder.Entity<SubwayLineEntity>().ToTable("SubwayLines", "trp");
            modelBuilder.Entity<UserTokenEntity>().ToTable("UserTokens", "trp");
            modelBuilder.Entity<UserProfileEntity>().ToTable("UserProfiles", "trp");
            modelBuilder.Entity<OrganizationEntity>().ToTable("Organizations", "trp");
        }
    }
}