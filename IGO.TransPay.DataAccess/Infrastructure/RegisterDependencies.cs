﻿namespace IGO.TransPay.DataAccess.Infrastructure
{
    using IGO.TransPay.DataAccess.Repositories;

    using Microsoft.Extensions.DependencyInjection;

    public static class RegisterDependencies
    {
        public static void RegisterDataAccessModule(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ITransportsRepository, TransportsRepository>();
            serviceCollection.AddTransient<IUsersRepository, UsersRepository>();
        }
    }
}